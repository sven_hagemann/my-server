import { Controller, Get, Param, Body, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { CalculationService } from './calculation/calculation.service';

@Controller()
export class AppController {
  constructor(
      private readonly appService: AppService, 
      private readonly calcService: CalculationService) { }

  // http://localhost:3000
  @Get()  // hier kommen die Get-Anfragen an
  getHello(): string {
    return this.appService.getHello();
  }

  // @Get()  // hier kommen die Get-Anfragen an
  // getHello(): string {
  //   return this.appService.getHello();
  // }

  // // http://localhost:3000/calculate/3
  // @Get('calculate/:number')
  // calculate(@Param() params): string {
  //   return (params.number + ' + ' + params.number + ' = ' + params.number * 2);
  // }

  // @Get('calculate/:number')
  // calculate(@Param() params, @Body('operation') operation): string {
  //   let result: string | number;
  //   console.log(operation);
    
  //   switch (operation) {
  //     case 'double':
  //       result = this.calcService.double(params.number);
  //       break;
  //     case 'square':
  //       result = this.calcService.square(params.number);
  //       break;
  //   }

  @Post('calculate/:number')
  calculate(@Param() params, @Body('operation') operation): string {
    let result: string | number;
    console.log('appController');
    
    switch (operation) {
      case 'double':
        result = this.calcService.double(params.number);
        break;
      case 'square':
        result = this.calcService.square(params.number);
        break;
    }
    
    result = ('Operation: ' + operation + ', Rechnung: ' + result);
    return JSON.stringify(result);
  }
}
